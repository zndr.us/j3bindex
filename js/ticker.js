function makeTicker(element){	
	$.post('./inc/ticker_get_data.php', function( result ) {							
		var data = jQuery.parseJSON(result);
		var content = '';
		for(var i=0;i<data.length;i++){	
			content += '<label class="item-sep">&#8226;</label>';		
			content += '<label class="item-name">'			+ data[i]['name'] + '</label>';	
			content += '<label class="item-volume">'		+ data[i]['volume'] + '</label>';	
			content += '<label class="item-buy">[buy]</label><label class="item-min">'	+ data[i]['min'] + '</label>';	
			content += '<label class="item-sell">[sell]</label><label class="item-max">'+ data[i]['max'] + '</label>';	
		}
		//set the content into the div
		element.html(content);				
		//make it a marquee
		element.marquee({					
			duration: 30000,//speed in milliseconds of the marquee					
			delayBeforeStart: 0,					
			direction: 'left',					
			duplicated: true
		});	
	});	
}