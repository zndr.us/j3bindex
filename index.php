<!doctype html>
<!--[if lt IE 7]> <html class="en no-js ie6 oldie ie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="en no-js ie7 oldie ie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="en no-js ie8 oldie ie" lang="en"> <![endif]-->
<!--[if gt IE 8]> <html class="en no-js newie ie" lang="en"> <![endif]-->
<!--[if !IE]><!--> <html class="en no-js" lang="en"> <!--<![endif]-->
<head>
    <title>Bearded BattleBears is an EVE Online corportion.</title>
    <meta name="author" content="Bearded BattleBears">
    <meta name="viewport" content="width=device-width">
    <script src="./js/751370585.js"></script>
	

	<meta property="og:url" content="http://www.beardedbattlebears.com/" />
	<meta property="og:type" content="article" />
	<meta property="og:site_name" content="Bearded BattleBears" />
	<link rel="stylesheet" href="./css/style.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<script>window.jQuery || document.write('<script src="./js/jquery-1.8.min.js"><\/script>')</script>
	<script src="./js/modernizr.custom.js"></script>
	<script src="./js/amplify-1.1.0.min.js"></script>

	<link rel="icon" type="image/png" href="img/j3b-logo-standalone.png">
</head>

<body  id="landing">

<div id="sticky-wrapper">
    <?php include'inc/nav.php';?>
    <!--<div class='ticker_footer'>
    <?php include 'inc/market.php';?>
    </div>-->
<main id="mainstuff" class="main-content" role="main">  
<section id="intro">
    <?php include'inc/intro.php';?>
</section>
<section id="what">
    <?php include'inc/what.php';?>
</section>
<section id="game">
    <?php include'inc/benefits.php';?>    
</section>
<section id="ourTeam">
    <?php include'inc/team.php';?>
</section>
</main>
<div id="sticky-push"></div>
</div>
<?php include'inc/footer.php';?>
	
<script src="./js/plugins.min.js"></script>
<script src="./js/scripts.min.js"></script>

<script>
    var jsLocalization = {
    "close": "Close",
    "next": "Next",
    "prev": "Previous"
}
</script>

</body>
</html>
