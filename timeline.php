<!doctype html>
<!--[if lt IE 7]> <html class="en no-js ie6 oldie ie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="en no-js ie7 oldie ie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="en no-js ie8 oldie ie" lang="en"> <![endif]-->
<!--[if gt IE 8]> <html class="en no-js newie ie" lang="en"> <![endif]-->
<!--[if !IE]><!--> <html class="en no-js" lang="en"> <!--<![endif]-->
<head>
    <title>Bearded BattleBears is an EVE Online corportion.</title>
    <meta name="author" content="Bearded BattleBears">
    <meta name="viewport" content="width=device-width">
    <script src="//cdn.optimizely.com/js/751370585.js"></script>
	

	<meta property="og:url" content="http://drawingaggro.com/" />
	<meta property="og:type" content="article" />
	<meta property="og:site_name" content="Bearded BattleBears" />
	<link rel="stylesheet" href="css/style.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="./js/jquery-1.8.min.js"><\/script>')</script>

	<script src="/js/modernizr.custom.js"></script>
	<script src="/js/amplify-1.1.0.min.js"></script>
    
	
</head>


<body  id="landing">
<div id="sticky-wrapper">
  <?php include 'inc/nav.php';?>

<main role="main" id="mainstuff" class="main-content">

   
    	    

<section id="game">
    <div class="container">
        <ul class="tmtimeline">
                    <li>
                      <time id="sprunkfleet" class="tmtime"><span>01/2013</span></time>
                      <div class="tmicon bg-pink fa-rocket "></div>
                      <div class="tmlabel">
                        <h2>Sprunk Fleets</h2>
                        <p>The beginning of 2013 was a peaceful time for TEST alliance. Stationed in Delve, they were living off the fat of the land.</p>

<p>Yet, this was not enough for Thesprunk. As a somewhat seasoned player at the time he found multiple new players who were having a hard time adjusting to life in null-sec. Combining his two favorite things began taking out �SprunkFleet DrunkFleets.� Taking a gaggle of new players and showing them the ins and outs of the game. Flying ships mostly from TESTFree they would roam and die. Their numbers grew, and people began looking to Thesprunk for more content.</p>

<p>In TEST alliance there were player formed groups called squads. Anyone could create a squad, and they varied in interest. People from various corporations could get together in a group to hang out and share their common interest. It was under this program that on March 13, 2013 the �JewBear BattleBeards/JewBeard BattleBears� (J3B) were formed. (It was never decided which name was correct)</p>
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>03/2013</span></time>
                      <div class="tmicon bg-cold-grey fa-home"></div>
                      <div class="tmlabel">
                        <h2>Move to Y-2ANO</h2>
                        <p>With a name and kind of a purpose, it was decided to move to a new home. Y-2ANO in Fountain was picked as the new promised land. Refinery and factory facilities close by and all the ratting your Naga could handle. The move was somewhat problematic as the squads logistics weren�t developed at the time. The move consisted of waiting until the route was clear and flying as many ships as you could. During the stay the squad continued to grown in both numbers and prowess. </p>
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>05/2013</span></time>
                      <div class="tmicon bg-yellow fa-home"></div>
                      <div class="tmlabel">
                        <h2>Move from Y-2ANO to TEG</h2>
                        <p>The squad eventually outgrew their first home. As the entrance system to Fountain it had traffic and wasn�t the most secure. It was then decided that TEG-SD, the entrance system to the Mermaid constellation would be home. With refining, a factory, mining opportunities, and secure pocket to rat in it was a perfect choice.</p>
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>05/2013 - 07/2013</span></time>
                      <div class="tmicon bg-red fa-rocket "></div>
                      <div class="tmlabel">
                        <h2>Pre-War TEG</h2>
                        <p>During the relatively short time of peace the squad grew leaps and bounds in power. Production chains were set up, jump freighters acquired, and combat toons gained skillpoints every day. It was a wonderful time to be a Battle Bear.</p>
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>07/2013</span></time>
                      <div class="tmicon bg-marine fa-rocket "></div>
                      <div class="tmlabel">
                        <h2>Fountain War</h2>
			<p>Much has been written about the Test/Goon Fountain War. For the BattleBears it wasn�t much different. Strategic operations to try and defend their homeland, coupled with production of ships to keep the gears of war turning. This culminated to the fall of 6VDT-H, the home of TEST Alliance. At the time it was the largest battle to occur in Eve online, previously held by the Battle of Asakai.</p>
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>07/31/13</span></time>
                      <div class="tmicon bg-marine fa-rocket "></div>
                      <div class="tmlabel">
                        <h2>Fall of 6VDT</h2>
                        <p>After the loss of the alliance headquarters a majority of the alliance fled the region. However, the Battle Bears would not give their homeland without a fight. Fortress TEG was activated. Any ship that was left in TEG would not be leaving TEG. Heavy losses were incured, but the BattleBears homeland was one of the last portions of Fountain to fall into enemy hands.</p>
						
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>08/2013</span></time>
                      <div class="tmicon bg-marine fa-rocket "></div>
                      <div class="tmlabel">
                        <h2>Fortress TEG</h2>
                        <p>After the loss of Fountain and the impending attack on Delve, TEST Alliance moved to low-sec. Soliara in the Aridia region was called home for a short while.</p>
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>08/2013</span></time>
                      <div class="tmicon bg-marine fa-rocket "></div>
                      <div class="tmlabel">
                        <h2>Fall of Fountain / Move to Soliara</h2>
                        <p>Living in Soliara was a difficult adjustment. A majority of BattleBears had spent their entire Eve life in null-sec. �What do you mean the gates will shoot me?� �You mean I get in trouble for killing this guy?� But the BattleBears stuck together. </p>
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>09/2013</span></time>
                      <div class="tmicon bg-marine fa-rocket "></div>
                      <div class="tmlabel">
                        <h2>J4UD/G-O</h2>
                        <p>After a short stay in low-sec, TEST moved to NPC Null-sec in Curse. While the alliance staged out of G-0Q86, the BattleBears decided to partially live from J4UD-J. At this point the squad had a player-alt corporation (for management of towers and production), squad owned assets in the form of researched blueprints, their own services (Teamspeak, jabber, etc), and a board of directors. What had started off as a group of drunk guys flying around space had turned into what looked oddly like a corporation. </p>
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>09/2013</span></time>
                      <div class="tmicon bg-marine fa-rocket "></div>
                      <div class="tmlabel">
                        <h2>"We're not gonna take it" Departure to N3 and Immensea/XS-K10</h2>
                        <p>As all good things must come to an end, so did the Bearded BattleBears time in TEST. A number of squad members were in the corporation Martyr�s Vengence (MV). MV was leaving TEST alliance to become a member of Nulli Secunda. A deal was struck with MV to bring the player-owned corporation (J3B) in as their �industrial alt-corp� to Nulli Teteris (the industrial alliance of Nulli Secunda), as well as all the BattleBear refugees from non-MV corps.</p>

<p>The BattleBears lived in XS-K10 during this time in Immensea. It was a quiet region when it came to enemy fleets. Carrier ratting, exploration, PAP links, and mining were the name of the game during this time. </p>
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>02/2014</span></time>
                      <div class="tmicon bg-marine fa-rocket "></div>
                      <div class="tmlabel">
                        <h2>Fall of Immesea : Move to Perrigen Falls</h2>
                        <p>Then a little thing called B-R5RB happened. Three jumps from the BattleBears home occured the largest, bloodiest battle in Eve history. (The title previously held by the Fall of 6VDT-H) As the battle was dominated by Titans and capitals, the Battle Bears played a minor role. Several members had carriers they jumped over to join in on the fun.</p>
<p>
The loss of B-R5RB eventually led to the loss of Immensea by Nulli Secunda, and another move. This time the BattleBears moved to Perrigen Falls. Perrigen Falls is considered the drone lands. Terrible place. The core of the alliance moved to a system significantly far away from where the BattleBears set up their new home in O-QKSM. A majority of members had slowly moved a majority of their characters into the �player-alt corporation.� </p>
                      </div>
                    </li>
                    <li>
                      <time class="tmtime"><span>02/2014</span></time>
                      <div class="tmicon bg-marine fa-home"></div>
                      <div class="tmlabel">
                        <h2>Wormhole? What wormhole?</h2>
                        <p>During the banishment to the drone lands, the Bearded BattleBears may or may not have tried to live in a wormhole. It may or may not have resulted in an eviction after a short amount of time culminating in several billions in losses.</p>
                      </div>
                    </li>
		    <li>
                      <time class="tmtime"><span>04/2014</span></time>
                      <div class="tmicon bg-marine fa-home"></div>
                      <div class="tmlabel">
                        <h2>Move to Brave (Bayuka)</h2>
                        <p>While living in Perrigen Falls and not trying to live in a wormhole, talks had begin with an up and coming alliance, Brave Collective. Anchored on the newbie-friendly Brave Newbies, Inc, the alliance was both growing and based on the values of having fun. Through the hard work of BattleBear leadership the �player-alt corporation� was accepted as a full-member corporation into the Brave Collective.</p>

<p>At this time the Brave Collective was stationed in Sendaya (low-sec). The BattleBears, as is tradition, stationed themselves in their own system close by. The welcome mat was put out, and Bayuka became home.</p>
                      </div>
                    </li>
		    <li>
                      <time class="tmtime"><span>04/2014</span></time>
                      <div class="tmicon bg-marine fa-rocket"></div>
                      <div class="tmlabel">
                        <h2>Burn Catch</h2>
                        <p>The time for the BattleBears to return to sov null-sec had come. Waiting for this moment, the BattleBears were able to flex their new muscle. What had once been a collection of drunk newbies roaming in frigates now wielded a majority share of the Brave Collective capital force. SBUs were placed, towers and stations reinforced, and Brave Collective was deemed the victors and new owners of Catch.</p>
                      </div>
                    </li>
		    <li>
                      <time class="tmtime"><span>04/2014</span></time>
                      <div class="tmicon bg-marine fa-home"></div>
                      <div class="tmlabel">
                        <h2>New Home (RNF)</h2>
                        <p>The BattleBears found a nice new home in RNF-YH. A factory facility with a refinery next door, it was the perfect place to set up shop for production. The jump bridge network also gave excellent access to alliance staging in GE-8JV for alliance fleets. It is in RNF-YH that the BattleBears have lived the longest. The Forrest Gump of Eve, they�ve seen the largest battles the game has to offer. Always at the right place at the right time, the Bearded Battlebears will know where to find the fun.</p>
                      </div>
                    </li>
                  </ul>
    </div>
</section>
</main>
        <div id="sticky-push"></div>
	</div>


	<footer class="main-content" id="footer">
		<?php include'inc/footer.php';?>
	</footer>
	
	<script src="/js/plugins.min.js"></script>

	<script src="/js/scripts.min.js"></script>


			<script>
				var jsLocalization = {
					"close": "Close",
					"next": "Next",
					"prev": "Previous"
				}
			</script>

</body>
</html>
