<!doctype html>
<!--[if lt IE 7]> <html class="en no-js ie6 oldie ie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="en no-js ie7 oldie ie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="en no-js ie8 oldie ie" lang="en"> <![endif]-->
<!--[if gt IE 8]> <html class="en no-js newie ie" lang="en"> <![endif]-->
<!--[if !IE]><!--> <html class="en no-js" lang="en"> <!--<![endif]-->
<head>
    <title>Bearded BattleBears is an EVE Online corportion.</title>
    <meta name="author" content="Bearded BattleBears">
    <meta name="viewport" content="width=device-width">
    <script src="//cdn.optimizely.com/js/751370585.js"></script>
	

	<meta property="og:url" content="http://drawingaggro.com/" />
	<meta property="og:type" content="article" />
	<meta property="og:site_name" content="Bearded BattleBears" />
	<link rel="stylesheet" href="css/style.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="./js/jquery-1.8.min.js"><\/script>')</script>

	<script src="./js/modernizr.custom.js"></script>
	<script src="./js/amplify-1.1.0.min.js"></script>
    
	
</head>


<body id="landing" class="subpage animate">
<div id="sticky-wrapper">
  <?php include 'inc/nav.php';?>

<main role="main" id="mainstuff" class="main-content">
		    
<section id="fc" class="content-section">
    <?php include'inc/fc.php';?>
</section>
<section id="srp" class="content-section">
    <?php include'inc/srp.php';?>
</section>
<section id="jf" class="content-section">
    <?php include'inc/jf.php';?>
</section>
<section id="pvp" class="content-section">
    <?php include'inc/pvp.php';?>
</section>
<section id="isk" class="content-section">
    <?php include'inc/isk.php';?>
</section>
<section id="meetups" class="content-section">
    <?php include'inc/meet.php';?>
</section>

</main>
        <div id="sticky-push"></div>
	</div>


	<footer class="main-content" id="footer">
		<?php include'inc/footer.php';?>
	</footer>
	
	<script src="./js/plugins.min.js"></script>

	<script src="./js/scripts.min.js"></script>


			<script>
				var jsLocalization = {
					"close": "Close",
					"next": "Next",
					"prev": "Previous"
				}
			</script>

</body>
</html>