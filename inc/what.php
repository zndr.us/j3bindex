<nav id="section-menu">
        <div class="container">
            <ul class="nav navbar-fixed-top">
                <li><a href="#what">Who is J3B?</a></li>
                <li><a href="#game">The Benefits of J3b</a></li>
                <li><a href="#ourTeam">Our Team</a></li>
            </ul>
        </div>
    </nav>        
    
    <div class="container">
        <div class="row">
            <div class="col-sm-8">            
                <h2>Who are the Bearded BattleBears?</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">                    
                <p>Bearded BattleBears [J3B] is a EVE Online corporation.  Since our creation in January of 2013 we have steadily grown over the past two years.  Now part of the Brave Alliance or "Hero" we strive to be the best in the Brave Alliance.  Our FC's are trained well and extremely active.  Very rarely are we not on some type of strat op, either camping a gate, assisting in alliance fleets, blopsing or doing some type of roaming gang.  But we don't stop there.  We have are involved with Faction Warfare, </p>
            </div>
            <div class="col-sm-6">
                <p>Industrial Production, Planetary Interaction, Capital Production, Mining, Ratting, Guns for Hire... We are a well balanced corporation with many different pilots involved in many different types of game play. Although we are primarily and EVE Online corporation.  We also enjoy playing other types of games such as FPS, Other MMO's, RPG, Strategy.  If you are interested in learning more about J3B and what we have to offer, please take the time to read about our benefits.</p>
            </div>
        </div>
        <div class="col-sm-12 text-center button">
            <a href="timeline.php" class="button--arrow landing">
                <span class="button--arrow-text">The History of J3B</span>
            </a>
        </div>
    </div>