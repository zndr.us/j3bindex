<?php 
date_default_timezone_set('UTC');

//  simple endpoint to 
//  get market data from eve-central
//  caches the response for :
$refreshEverySecs = 300;      // 5 min
//caches is stored in this directory
$tmpdir           = sys_get_temp_dir();
//as a json file named
$cacheFileName    = $tmpdir.'/marketdata.json';

//this queries data from 
$system           = 30000142;  
// jita, this is the eve system id

//these are the items to lookup the price data for
$items = array();
$items[29668] = 'PLEX';
$items[4358]  = 'Exotic Dancers, Male';
$items[17765] = 'Exotic Dancers, Female';
$items[13267] = 'Janitor';
$items[16273] = 'Liquid Ozone';
$items[4310]  = 'Tornado';
$items[16274] = 'Helium Isotopes';
$items[17887] = 'Oxygen Isotopes';
$items[17888] = 'Nitrogen Isotopes';
$items[17889] = 'Hydrogen Isotopes';
$items[34]    = 'Tritanium';
$items[35]    = 'Pyerite';
$items[36]    = 'Mexallon';
$items[37]    = 'Isogen';
$items[38]    = 'Nocxium';
$items[39]    = 'Zydrine';
$items[40]    = 'Megacyte';
$items[11399] = 'Morphite';


//

$refresh   = false;
//see if we need to refresh
	if(file_exists($cacheFileName)){	
		//got a file, check time stamp
		$lastMod = filemtime ($cacheFileName);	
		//get current time
		$currentTs = getdate(time())[0];
		//check if stale
		if($lastMod < ($currentTs-$refreshEverySecs) ){
			//need refresh
			$refresh = true;
		}
	}else{
		//never gotten any data
		$refresh = true;
	}
	
	//if we need to refresh
	if($refresh){			
		//grab keys
		$keys      = implode(',', array_keys ($items));
		
		//call eve-central
		$response = file_get_contents('http://api.eve-central.com/api/marketstat/json?typeid='.$keys.'&usesystem='.$system);	
		//save response as cached data
		$fcache = fopen($cacheFileName, 'w');
			fwrite($fcache, $response);
		fclose($fcache);
	}
		
	//this is the dto to send down to the javascript (simplified)
	class price{
		public $id = 0;
		public $name = "";
		public $volume 		= 0.0;
		public $max			= 0.0;
		public $min			= 0.0;	
	}
	
	function formatNumber($value, $numDec){		
		if($value > 10000000){			
			return number_format($value/10000000, $numDec).'M';		
		} else if ($value > 1000){
			return number_format($value/1000, $numDec).'K';		
		} else {
			return number_format($value, $numDec);
		}		
	}
	//get the data from the cached file
	$response = json_decode( file_get_contents($cacheFileName) );	
	$tmp = array();
	//for each item in the response, get the name and populate a 'price' object
	foreach( $response as &$item ){
		//we'll use the buy price I guess
		$id = $item->buy->forQuery->types[0];
		$p = new price();
		$p->id = $item->buy->forQuery->types[0];
		
		$p->name = $items[$id];
		$p->volume		  = formatNumber($item->sell->volume, 0);		 
		$p->max			  = formatNumber($item->buy->max, 2);		
		$p->min			  = formatNumber($item->sell->min, 2);				 
		array_push($tmp, $p);	
	}
 	//encode and return the value
	echo json_encode($tmp, JSON_PRETTY_PRINT);
?>
