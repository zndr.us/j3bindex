<div class="container">
    <div class="row">
            <article class="col-lg-12">
                <h2 class="fc-blue">Making ISK</h2>
                <h3 class="fc-blue">What are the best methods</h3>
                <p>In reality it depends on what you want to do.  There are several different methods as to which you can make isk.</p>
                <ol>
                    <li>Ratting (ship/carrier)</li>
                    <li>Marketing PVP</li>
                    <li>BPO's Research</li>
                    <li>Mining</li>
                    <li>Planetary Interaction</li>
                    <li>Industrial Production</li>
                    <li>Exploration</li>

                    
                </ol>
                <h3 class="fc-blue">Ratting</h3>
                <p>Ratting is nothing more than grinding NPC's.  If you ratting in a carrier you will probably make the most isk for your time.  It can also be a good side project while you are doing other things.  Although if you are not running dual monitors we would not suggest you do this.</p>
                <h3 class="fc-blue">Market PVP</h3>
                <p>What is better then Market PVP.  Import what is needed and sell.  Remember to buy low and sell high.  Although to high will get you into trouble.  Keep 10-15% above market price.</p>
                <h3 class="fc-blue">BPO's</h3>
                <p>Whether you want to have your own vast collection to research and sell copies, or produce them for your own industrial line to create ships and modules for the market, it all starts with a Blueprint Original. Taking the time to research a blueprint and marking it up for the time spent is a great way to make money at a super passive level.</p>
                <h3 class="fc-blue">Mining</h3>
                <p>The simplest and most relaxing way of making money. You are essentially mining the life for EVE ship and module creation. It can take an extensive setup to make billions, but is a nice low SP beginning income. Doing so in 0.0 space just like ratting does come with its dangers of roaming gangs and hotdroppers so be watchful.</p>
                <h3 class="fc-blue">Planetary Interaction</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <h3 class="fc-blue">Industrial Production</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <h3 class="fc-blue">Exploration</h3>
                <p>Exploration can be a great source of income.  If you enjoyed the game of mine sweeper growing up then you will like how CCP changed the Exploration methods.  Hacking Data & Relic sites can be extremely rewarding, however if you are not paying attention in local, or you can't hack the site.  You are either going to go boom, or your can goes boom.  The BEST way to hack these sites is to have T2 rigs.  The higher your abilities the better chances you have of hacking the cans.</p>
                
            </article>
        </div>
    </div>