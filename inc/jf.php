<div class="container">
    <div class="row">
            <article class="col-lg-12">
                <h2 class="fc-blue">Jump Frieghter Services</h2>
                <h3 class="fc-blue">When are they done?</h3>
                <p>Runs are made twice a week.  Mostly done on Wednesdays & Saturdays unless there are RL issues that take priority.</p>
                <h3 class="fc-blue">Are there requirements?</h3>
                <p>Yes there are.  Below is a list of them.</p>
                <ol>
                    <li><span>MUST</span> be a member of J3B</li>
                    <li>Contracts must go to a specific char otherwise they will be rejected</li>
                    <li>It will cost you 650isk per/m3</li>
                    <li>25m isk minimum reward</li>
                    <li>325k m3 maximum contract size</li>
                    <li>NO containers (We don't care how many BPC's/BPO's you have)</li>
                    <li>NO collateral</li>
                    <li>Contracts ENTERING (Op Sec System) have a 1 billion isk worth limit (use <a href="http://www.evepraisal.com">EVEPraisal.com)</a></li>
                    <li>Contracts LEAVING (Op Sec System) have no limit</li>
                    <li>Set maximum time to accept and to complete</li>
                    <li>We have several different systems to which we will deliver / pick-up</li>
                </ol>
                <h3 class="fc-blue">When will I get my stuff?</h3>
                <p>Contracts may not complete until up to 48-hours after scheduled run days due to Jump Fatigue changes and me taking on 2 more JF pilots (that aren't me) for this to be done in a more acceptable time. Hopefully it will never take that long to complete but just expect them to not be delivered same-day anymore.</p>
                <h3 class="fc-blue">Questions?</h3>
                <p>If you have any questions regarding our Jump Frieghter Services please make sure to contact Mr. Zigglesworth</p>
            </article>
        </div>
    </div>