<div class="container">
        <h2>PVP (Player vS. Player)</h2>
        
        <div class="row">
            <article class="col-sm-4">
                <img src="img/gate.jpg" />
                <h3>Gate Camping</h3>
                <p>Depending on what type of camping your doing, will depend on how much you enjoy it.  You can either camp the gate for Strat Ops, or join up on one of the nightly camps.</p>
            </article>
            <article class="col-sm-4">
                <img src="img/cloak.jpg" />
                <h3>Blopsing</h3>
                <p>Although we do not blops much these days do to jump fatigue, we still get out there and put povi block in thier place.  </p>
            </article>
            <article class="col-sm-4">
                <img src="img/roam.jpg" />
                <h3>Roaming</h3>
                <p>If you don't know what roaming is...uninstall eve.  Simply put, you roam around your space looking for targets, or you can decide to go into enemy space and welp on them.</p>
            </article>
            <article class="col-sm-4">
                <img src="img/defense.jpg" />
                <h3>System Defense</h3>
                <p>If your new to PVP and not really sure what you are doing.  Join one of the many system defense fleets that are ongoing throughout the day.  There is always content and always a need for fresh blood.</p>
            </article>
            <article class="col-sm-4">
                <img src="img/solo.jpg" />
                <h3>Solo PVP</h3>
                <p>It's all you.  Know your ship, know your skills, know your targets.  Nothing worse then going out in a nice shiny and getting welped because you got to cocky.</p>
            </article>
            <article class="col-sm-4">
                <img src="img/caps.jpg" />
                <h3>Capital Fleets</h3>
                <p>Mainly used for pos bashing and system defense, capital fleets or engagements are one of the most intense fights on grid.  Billions of isk hang in the balance of the support fleets ability to do their jobs.</p>
            </article>
        </div>    
    </div>