<div class="container">
        <h2>The Benefits of J3B</h2>
        
        <div class="row">
            <article class="col-sm-4">
                <img src="img/fc.jpg" />
                <p><b>FC Program</b></br>J3B is an extremely active PVP/Industrial corporation within EVE Online.  Because of this we are constantly helping to develop our pilots into becoming as active as they want to be.    </p>
            </article>
            <article class="col-sm-4">
                <img src="img/srp.jpg" />
                <p><b>SRP Program</b></br> J3B SRP program is what helps our pilots stay active.  Our FC tiers provide us with different levels of SRP, which makes it easier for our newBros to be a bit more active then they usually would without this type of isk support.</p>
            </article>
            <article class="col-sm-4">
                <img src="img/services.jpg" />
                <p><b>JF Services</b></br>  There is always a concern when living in null-sec of how to get goods from high-sec.  Not to mention the BFing going on in null sec sometimes.  J3B delivers goods twice a week to it's members.  If you ever need something from carebear space.  This is your best option.</p>
            </article>
	    <article class="col-sm-4">
                <img src="img/pvp.jpg" />
                <p><b>PVP</b></br>  Our dedication to PVP is one of the main reasons why our members stick around.  If we are not helping others defend their space, we are actively blopsing or roaming around.</p>
            </article>
	    <article class="col-sm-4">
                <img src="img/industrial.jpg" />
                <p><b>ISK Making Opportunity</b></br> We have members who do all sorts of things within EVE Online.  BPO research, Capital Production, Mining, Freight Transport, Planetary Interaction... We are active in every way possible to make isk.</p>
            </article>
	    <article class="col-sm-4">
                <img src="img/community.jpg" />
                <p><b>Growing Community</b></br>Having an active community is always great. But J3B does their best to host local meetups whenever possible.  The better we know each other the closer the community will be.</p>
            </article>
        </div>    
        <div class="col-sm-12 text-center button">
            <a href="benefits.php" class="button--arrow landing">
                <span class="button--arrow-text">Read more about what we can offer</span>
            </a>
        </div>
    </div>