<?php 
class price{
	public $id = 0;
	public $name = "";
	public $avg = 0.0;
}

$items = array();
$items[18] = 'Plagioclase';
$items[19] = 'Spodumain';
$items[20] = 'Kernite';
$items[21] = 'Hedbergite';
$items[22] = 'Arkonor';
$items[1223] = 'Bistot';
$items[1224] = 'Pyroxeres';
$items[1225] = 'Crokite';
$items[1226] = 'Jaspet';
$items[29668] = 'PLEX';

$keys = implode(',', array_keys ($items));

$system = 30000142;  //jita
$cacheFileName = 'marketdata.json';

$refresh = false;
$tmp = getdate();
echo var_dump($tmp);
$currentTs = $tmp[0];
$refreshEverySecs = 3600;  // 1 hour

if(file_exists($cacheFileName)){	
	$lastMod = filemtime ($cacheFileName);	
	if($lastMod < ($currentTs-$refreshEverySecs) ){
		$refresh = true;
	}
}else{
	$refresh = true;
}

if($refresh){		
	$response = file_get_contents('http://api.eve-central.com/api/marketstat/json?typeid='.$keys.'&usesystem='.$system);				
	$fcache = fopen($cacheFileName, 'w');
		fwrite($fcache, $response);
	fclose($fcache);
}

$data = json_decode(file_get_contents($cacheFileName));

$retval = array();
foreach( $data as &$item ){
	$id = $item->all->forQuery->types[0];
	$p = new price();
	$p->id = $item->all->forQuery->types[0];
	$p->name = $items[$id];
	$p->avg  = $item->all->avg;
	array_push($retval, $p);	
}
echo json_encode($retval, JSON_PRETTY_PRINT);
?>
 