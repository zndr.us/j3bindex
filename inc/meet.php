<div class="container">

<div class="row">
<article class="col-lg-12">
    <h2>J3B Community</h2>
<h3>A Growing Community</h3>
                <p>One of J3B's most precious assests is each other, and because of that belief we make sure to build a healthy community.  Yes we are all a bunch of number crunching nerds, and sure other corps in the alliance may call us elitist (haters gonna hate) but when it comes down to it.   We are there for each other.   We take the time to reach out if we notice a member is struggling either in-game or reallife.  Our members are important to us.  I think it is also important to note that J3B is and will always be LGBT friendly.  </p>
                <p>With that being said we are not just a EVE Online corporation, we enjoy playing other games as well.  We are into FPS, Strategy, RP, Other MMO's, and hell even Minecraft from time to time.  Either way we like to do other things.  So come join us and become not just part of the J3B community, but the J3B Family.</p>
                <p>There is also this awesome site that you can go to called <a href="http://www.evemeet.net">EVEmeet.net</a> Where other players are setting up meet events.  It's a great way to build new friendships.  At the end of the ladies and gents, this is just a game.  Real Life is ALWAYS more important.  So don't forget to get out there and meet new people.</p>
                <p>If you need to talk to someone and none of us are online at the time.  Please join the in-game channel <span class="fc-blue">#broadcastforreps</span>.  </p>
              
</article>  
        </div>
</div>