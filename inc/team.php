<div class="texture"></div>
      <a class="anchor" id="ourTeamAnchor"></a>
      <div class="container">
        <div class="row"><div class="col-sm-12"><h3>Our Team</h3></div>
        <div class="row">
	  <div class="col-sm-6 teamMemberInfo">
            <img class="teamImage" src="img/kelnon.png" alt="" />
            <div class="col-sm-7 teamText">
            <h4><span><a href="https://gate.eveonline.com/Profile/Kelnon%20Tealth">Kelnon Tealth</a></span> / CEO</h4>
            <p>Kelnon became the CEO after the founder needed to take a break.  His roles vary from day to day based on what the corporation needs at the moment.  He can often be found sitting in a Naglfar awaiting the next time he gets head shot.</p>
            </div>
          </div>
          <div class="col-sm-6 teamMemberInfo">
            <img class="teamImage" src="img/sprunk.png" alt="" />
            <div class="col-sm-7 teamText">
            <h4><span><a href="https://gate.eveonline.com/Profile/Thesprunk">TheSprunk</a></span> / IT Manager</h4>
            <p>Sprunk is the founder of the Bearded BattleBears, having to step away for things IRL. Sprunk continued to stay dedicated to the development and progress of his Bears.  Now back he is running everything that is technical for J3B.  Welcome back Sir Sprunk you were missed. </p>
            </div>
          </div>
	  <div class="col-sm-6 teamMemberInfo">
            <img class="teamImage" src="img/mrziggle.png" alt="" />
            <div class="col-sm-7 teamText">
            <h4><span><a href="https://gate.eveonline.com/Profile/Mr%20Zigglesworth%20Zigglesworth">Mr. Zigglesworth Zigglesworth</a></span> / Logistics Director</h4>
            <p>Mr Zigglesworth's job involves moving everything from point A to point B.  He has been doing this job since he had to do it in Iterons and has moved up to multiple jump freighters.  A testament to his dedication - even after a major surgery, he did not miss his regularly scheduled run to Jita and back, he moved it through the pain (and pain killers).</p>
            </div>
          </div>
	  <div class="col-sm-6 teamMemberInfo">
            <img class="teamImage" src="img/kain.png" alt="" />
            <div class="col-sm-7 teamText">
            <h4><span><a href="https://gate.eveonline.com/Profile/Kain%20Keiley">Kain Keiley</a></span> / Flex Director</h4>
            <p>Kain's job is less of any particular things and more of everything.  His job is to do what the corporation needs right that moment.  He can often be found yelling "Goddammit Kelnon" right before he does something absolutely critical, and does it well.</p>
            </div>
          </div>
	  <div class="col-sm-6 teamMemberInfo">
            <img class="teamImage" src="img/praal.png" alt="" />
            <div class="col-sm-7 teamText">
            <h4><span><a href="https://gate.eveonline.com/Profile/Praal">Praal</a></span> / Towers Director</h4>
            <p>Praal is the man who can manage any insane number of towers.  When Phoebe hit, he let it roll off his back like water and kept the farms going.  Praal has told us many times, he loves towers because they do exactly what you expect every time and this is his great contribution to the corporation.</p>
            </div>
          </div>
	  <div class="col-sm-6 teamMemberInfo">
            <img class="teamImage" src="img/nathan.png" alt="" />
            <div class="col-sm-7 teamText">
            <h4><span><a href="https://gate.eveonline.com/Profile/Nathan%20Reklaw">Nathan Reklaw</a></span> / HR Director & Diplo Team </h4>
            <p>Nathan defines human resources, he manages to run both recruitment and internal issues without so much as breaking a sweat.  He goes through more applications with a full background check than any person ever should, and does it with a smile.  Nathan's hardest moments aren't dealing with most members or recruitment, it's the human resources problems Kelnon creates.</p>
            </div>
          </div>
	  <div class="col-sm-6 teamMemberInfo">
            <img class="teamImage" src="img/foobar.png" alt="" />
            <div class="col-sm-7 teamText">
            <h4><span><a href="https://gate.eveonline.com/Profile/Foobar%20Raboof">Foobar raboof</a></span> / Developer </h4>
            <p>Foobar is part of our Development team.  He does all that behind the scenes coding that none of us understand.  When he's not coding he's typically out leading fleets and welping on whoever gets in his way.</p>
            </div>
          </div>
	  <div class="col-sm-6 teamMemberInfo">
            <img class="teamImage" src="img/tehjackal.png" alt="" />
            <div class="col-sm-7 teamText">
            <h4><span><a href="https://gate.eveonline.com/Profile/TehJackal">TehJackal</a></span> / Brand Coordinator </h4>
            <p>TehJackal is our Brand Coordinator.  His sole responsibility is to make Bearded BattleBears look sexy.  You won't really find him online much these days as he is busy currently working on a super secret project.  Although when he is in fleets you can count on him to be flying something that he can easily toss away.</p>
            </div>
          </div>
        </div><!-- END Row -->
      </div><!-- END Container -->