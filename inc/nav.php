<header class="main-content noprint" id="mainheader-responsive">
	<div>
	<a id="mainlogo" href="http://www.beardedbattlebears.com"><img src="img/j3b-logo.png" alt="J3B Bearded BattleBears" ></a>
	    
	</div>
	<a id="nav-toggle" href="#navigation"><span aria-hidden="true" class="nav-toggle-icon"></span><span class="visuallyhidden">Menu</span></a>
    </header>
    <section id="navigation-responsive">
		<nav id="mainnav" role="navigation" >            
		    <ul class="clearfix">
			    <li><a href="http://ecm.beardedbattlebears.com/">ECM</a></li>
			    <!--<li><a href="https://dscan.beardedbattlebears.com/">D-Scan</a></li>
			    <li><a href="https://wiki.beardedbattlebears.com/">Wiki</a></li>
			    <li><a href="https://orders.beardedbattlesbears.com">Orders</a></li>-->
			    <li><a href="#">Killboards</a><ul class="l4">
				<li><a href="https://zkillboard.com/corporation/98206783/">J3B Board</a></li>
				<li><a href="https://zkillboard.com/alliance/99003214/">Brave Board</a></li>
			    </ul>
			    
			    </li>
			    <li><a href="#">Community</a><ul class="l4">
				<!--<li><a href="http:/forums.beardedbattlebears.com">J3B Forums</a></li>-->
				<li><a href="http://www.reddit.com/r/jewbears">J3B Reddit</a></li>
				<li><a href="http://www.reddit.com/r/bravenewbies">Brave Reddit</a></li>
			    </ul>
			    </li>
			    <li><a href="#">Media</a><ul class="i4">
			    <li><a href="https://www.youtube.com/channel/UCDd2lOlO1RapiLrVTJ_83nw">Videos</a></li>
		    </ul>
		</nav>
    </section>
