<div class="texture"></div>
    <div id="vidcontainer">
        <div class="inner">
            <img src="img/sliderBg.jpg">
        </div>
    </div>

    <div class="container">
        <div class="row">

            <div class="col-sm-5 flip">
                <h2>Bearded BattleBears</h2>
                <h3>We do it all!</h3>
                <div class="col-sm-12">
                    <a href="http://ecm.beardedbattlebears.com" target="_blank" class="button--arrow landing blue">
                        <span class="button--arrow-text">Join J3B Today</span>
                    </a>
                      <!--<div class="already">
                        <p>Need to Plex?</p>
                        <p><a target="_blank" href="www.markeedragon.com/affiliate.php?id=265&blueirect=index.php?cat=4">Support J3B and Plex!</a></p>
                    </div>-->
                </div>
            </div>
            <div class="col-sm-7">
                <div class="video-container">
                    <iframe width="500" height="280" src="https://www.youtube.com/embed/xmrU2vWLyNU" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>