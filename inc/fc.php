<div class="texture"></div>
<nav id="section-menu">
        <div class="container">
            <ul class="nav">
                <li><a href="#fc">FC Program</a></li>
                <li><a href="#srp">SRP Program</a></li>
                <li><a href="#jf">JF Services</a></li>
                <li><a href="#pvp">PVP</a></li>
                <li><a href="#isk">Making ISK</a></li>
                <li><a href="#meetups">Meetups</a></li>
            </ul>
        </div>
    </nav>        
    
    <div class="container">

        <div class="row">
            <div class="col-sm-8">
                <h2>FC Program</h2>
                <p>Our FC Program is probably one of most respected programs within the Brave Alliance.  So much so Brave's current FC Program was modeled after ours.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h3>SUBCAPITAL</h3>
                <h4 class="fc-blue">TIER 1 - Teady Bear</h4>
                 <p><span class="fc-blue">Class / Doctrine:</span> Frigates, Destroyers</p>
                <p><span class="fc-blue">SRP:</span> 5m ISK</p>
                <p><span class="fc-blue">Eligibility:</span> All active members of J3B</p>
            </div>
            <div class="col-sm-4">
                <h3>CAPITAL</h3>
                <h4 class="fc-green">TIER 1 - Training Level</h4>
                 <p><span class="fc-green">Class / Doctrine:</span> Must have capital FC present</p>
                <p><span class="fc-green">SRP:</span> N/A</p>
                <p><span class="fc-green">Eligibility:</span> Request entry into the capital training program.  Demonstrate competence in cyno lighting fleet organization, jump planning, and capacitor chaining.</p>
            </div>
            <div class="col-sm-4">
                <h3>LOGISTICS</h3>
                <h4 class="fc-red">TIER 1 - General Hauler</h4>
                 <p><span class="fc-red">Class / Doctrine:</span> Can Accept general contracts in high-sec, and assist with move ops when apprived by logistics FC.</p>
                <p><span class="fc-red">Eligibility:</span> Request Entry into the logistics program.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <h4 class="fc-blue">TIER 2 - KOALA BEAR</h4>
                 <p><span class="fc-blue">Class / Doctrine:</span> Frigates, Destroyers</p>
                <p><span class="fc-blue">SRP:</span> 25m ISK</p>
                <p><span class="fc-blue">Eligibility:</span> All active members of J3B</p>
            </div>
            <div class="col-sm-4">
                <h4 class="fc-green">TIER 2 - Capital Support</h4>
                 <p><span class="fc-green">Class / Doctrine:</span> Triage, General Ops</p>
                <p><span class="fc-green">SRP:</span> What is needed</p>
                <p><span class="fc-green">Eligibility:</span> Show competence in target calling, jump mechanics, drone mechanics, warp mechanics, fleet organization, and capacitor management.  Request review for promotion.</p>
            </div>
            <div class="col-sm-4">
                <h4 class="fc-red">TIER 2 - Logistics Jump FC</h4>
                 <p><span class="fc-red">Class / Doctrine:</span> Can Accept general contracts in high-sec, and assist with move ops when approved by logistics FC</p>
                <p><span class="fc-red">Eligibility:</span> Request Entry into the logistics program.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h4 class="fc-blue">TIER 3 - PANDA BEAR</h4>
                 <p><span class="fc-blue">Class / Doctrine:</span> ONI, VNI, ABC, Nuetgeddon</p>
                <p><span class="fc-blue">SRP:</span> 100m ISK</p>
                <p><span class="fc-blue">Eligibility:</span> Run 15 fleets, run 2 training ops, and request review for promotion.</p>
            </div>
            <div class="col-sm-4">
                <h4 class="fc-green">TIER 3 - Capital FC</h4>
                 <p><span class="fc-green">Class / Doctrine:</span> Siege, Supers, Slowcats (at owner discretion, no SRP), traps, coordinate colaition ops.</p>
                <p><span class="fc-green">SRP:</span> N/A</p>
                <p><span class="fc-green">Eligibility:</span> Show competence in fighters / figher-bombers, siege mechanics, area of effect weapons, fleet coordination with subcapitals, and armor/shield mechanics.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h4 class="fc-blue">TIER 4 - POLAR BEAR</h4>
                 <p><span class="fc-blue">Class / Doctrine:</span> HAC, Non-Faction BS, Fauxcats</p>
                <p><span class="fc-blue">SRP:</span> 200m ISK</p>
                <p><span class="fc-blue">Eligibility:</span> Run 30 fleets, run 2 training ops, and request review for promotion.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h4 class="fc-blue">TIER 5 - GRIZZLY BEAR</h4>
                 <p><span class="fc-blue">Class / Doctrine:</span> Grizzly w/ triage, fauxcats, can call capital support</p>
                <p><span class="fc-blue">SRP:</span> What is needed</p>
                <p><span class="fc-blue">Eligibility:</span> Run 50 fleets, run 5 training exercises, requires Tier 2 Capital endorsement, and request review.</p>
            </div>
        </div>
    </div>