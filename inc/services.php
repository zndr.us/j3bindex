<div class="container">
        <h2>Account Management</h2>
        
        <div class="row">
            <article class="col-sm-6">
                <h3>How do I register an account?</h3>
                <p>For the boxed version, go to <a href="https://secure.eveonline.com/Activation/">https://secure.eveonline.com/Activation/</a> and follow these steps:</p>
                <ol>
                    <li>Enter the code that came with your EVE Online retail box</li>
                    <li>Select the New Player option to create a new account</li>
                    <li>Enter your desired username and basic account information</li>
                    <li>You will receive your account information and details about your special bonuses for the boxed version</li>
                </ol>
                <p>For the downloadable version, go to https://secure.eveonline.com/buy/ and follow these steps:</p>
                <ol>
                    <li>Enter your desired username and basic account information</li>
                    <li>Select a payment method and enter the necessary billing information</li>
                    <li>An e-mail containing further information will be sent to the email address registered with your account</li>
                </ol>
            
                <h3>How often do I need to renew my account?</h3>
                <p>Unless you are using game time cards/single payment methods, cancel your account or desire to change your payment option, you will not need to manually renew your account. Once you�ve selected a subscription method, billing interval and have completed your account registration, your subscription will be renewed automatically.</p>
            
                <h3>Paying for EVE</h3>
                <p>Information on subscription fees and methods may be found in the associated knowledge-base articles, in English.</p>
                <ul>
                    <li><a href="https://community.eveonline.com/support/knowledge-base/article.aspx?articleId=787">Subscription costs</a></li>
                    <li><a href="https://community.eveonline.com/support/knowledge-base/article.aspx?articleId=21">Payment options and methods accepted</a></li>
                </ul>
            </article>

            <article class="col-sm-6">
                <h3>How can I modify my account information?</h3>
                <p>Go to the <a href="https://secure.eveonline.com/">Account Management</a> page and log in with your username and password. When you have logged in successfully, you will be presented with general options for your account and also these options in the menu:</p>
                <ul>
                    <li>Account: View and update your account details and change your password</li>
                    <li>Game Time: Modify your billing information, monitor or extend your subscription</li>
                    <li>Services: Buy PLEX, transfer a character, or trade EVE Time Codes</li>
                </ul>

                <h3>How can I retrieve my password if I lose it?</h3>
                <p>Go to the <a href="https://secure.eveonline.com/">Account Management</a> page. Click the Forgot your password? option under the Log in button. Enter your username and e-mail address and your password will be sent to you within minutes. The e-mail address must match the one specified in the account profile. If this e-mail address is not current, you will need to contact customer service and verify that you are the account holder before the information can be sent to you.</p>

                <h3>I am going to be abroad for an extended period of time without access to the Internet. I would like to temporarily close my account while I�m away. How do I do this and will it cause me to lose my characters or equipment?</h3>
                <p>All information about your characters and their belongings is stored on our servers. If your account is temporarily disabled for any reason, this data will be preserved for several months. You can reopen your account at any time through the <a href="https://secure.eveonline.com/">Account Management</a> page and restart your subscription.</p>
            </article>
        </div>
    </div>